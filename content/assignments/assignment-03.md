+++
title = "Assignment 3 - CAD"
description = ""
+++

I tried out different OSS packages that I haven't used before (as I'm mostly using Adobe products)


# 2D
## Inkscape

As I'm an Adobe Illustrator user, I wanted to have same keyboard shortcuts in InkScape, and you can easily set this from preferences.

![](./media/inkscape-keyboard-shortcuts.png "inkscape keyboard shortcuts")

## Gimp

For Gimp this was not as easy, but [GimpPs](https://github.com/doctormo/GimpPs) can make Gimp behave 
more like Photoshop.

![](./media/gimpps.png "GimpPs")


## Paper.JS

![](./media/paperjs1.gif "PaperJS")
![](./media/paperjs2-params.gif "PaperJS")

## Editing SVG in VSCode (extension)

![](./media/a3_vscode_svg.gif "Editing SVG in VSCode")

# 3D

## Blender

Blender feels very powerful - however it is not primarily suited for CSG modeling, but can achieve good results via careful configuration and use of 3rd party addons.

Here are my scene (right) + export settings (left).

[Example scene (d/l)](./media/blender-csg-example.blend)

![](./media/blender-scene-export-unit-settings.png "Blender settings")

Live parameters for non-destructive editing

![](./media/blender-parametric.gif "Blender parameters")
![](./media/blender-custom-properties.gif "Blender parameters")

![](./media/blender-ultimaker-final.png "Blender export in Ultimaker")

## LibFive Studio

The live-manipulation + back-propagating of the parameters back to the script got me motivated to try 
out the libfive Studio. There are a few examples shipped with the app, but the syntax is very strange to me as I've never learned Scheme/Lisp.

![](./media/libfive-studio-tutorial.png "LibFive Studio")

It seems that without adjusting any settings, the exported units are read as millimeters in Ultimaker.

![](./media/libfive-ultimaker-units.png "LibFive to ultimaker unit problem")

## OpenJSCad

As I'm not a too experienced developer and most proficient in Javascript, I wanted to see if there are any JS-based CAD tools available, and found OpenJSCad. 
You can define custom parameters like in Blender and LibFive, and then define both code and data that make use of those parameters. 

![](./media/openjscad.gif "OpenJSCad parametric")
![](./media/openjscad-cura.png "OpenJSCad cura")