+++
title = "Assignment 1 - project management"
description = "This is a1"
+++

<p>
First I created a GitLab account, and added my public SSH key to my account.
</p>
<code>
<ul>
<li>cd ~/.ssh
<li>cat id_rsa.pub
<li>cd ~/projects/
<li>brew install hugo
<li>hugo new site FAB-DOC
<li>cd FAB-DOC
<li>git submodule add https://github.com/leonstafford/accessible-minimalism-hugo-theme themes/accessible-minimalism
<li>hugo new posts/my-first-post.md
<li>hugo server
<li>nano .gitlab-ci.yml
	</ul>
</code>

<p>
My configuration inside the .gitlab-ci.yml in order to set up GitLab Pages publishing
</p>

<div class="textfile" data-filename=".gitlab-ci.yml">
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
</div>
<p>
Next up I initialized a git repository for version control and publishing
</p>
<code>
<ul>
<li>git init
<li>git remote add origin https://gitlab.com/eeropic/fablab-doc.git
<li>nano .gitignore
</ul>
</code>
<p>
My configuration for ignoring certain files from the git repository - on Mac you might need to ignore the automatically created filesystem's .DS_Store files
</p>
<div class="textfile" data-filename=".gitignore">
/public
.DS_Store
**/.DS_Store
</div>
<p>
Here's my configuration for the Hugo static site generator	
</p>
<div class="textfile" data-filename="config.toml">
baseURL = "https://eeropic.gitlab.io/fablab-doc/"
languageCode = "en-us"
title = "Eero Pitkänen - Digital Fabrication course documentation"
theme = "accessible-minimalism"

[params]
heading1 = "Eero Pitkänen"
subheading = "Digital Fabrication course documentation"
description = "Hello world! This is my newborn documentation site work in progress."	
</div>

<p>
Finally I add all the new files to the repository, commit them and push them to the Gitlab
</p>


<code>
<ul>
<li>git add .
<li>git commit -m "Initial commit"
<li>git push -u origin master
<li>

</ul>
</code>

<p>
	Next I created some simple styling for documenting steps taken in terminal and file contents seen above.
	The "console" divs have the dollar sign as a pseudo-element so they won't be copied when selecting text.
</p>