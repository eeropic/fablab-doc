+++
title = "Assignment 4 - Computer-Controlled Cutting"
description = "Computer-Controlled Cutting"
+++

## Modular plywood design puzzle experiment

I wanted to create a design that would also make use of the depth of the material, so I chose to use 6mm thick plywood and design all the elements using a 6 mm x 6 mm grid. This allows combining the pieces in any orientation.

![](./media/a4/fusion_grid.jpg "")

I created a test piece in Fusion 360 that allows modifying of the finger joint ledge and gap dimensions. 

![](./media/a4/thickness.jpg "")

![](./media/a4/blender-pieces.jpg "")

However for this design it was more straightforward to design all the pieces in 2D, so I created rest of the pieces in Illustrator - and then tried different assemblies in Blender before printing.

Blender allows you to make instances of objects in the same way how you define "symbols" in Illustrator or components in Fusion - so that changes made to single component propagate to all instances.

![](./media/a4/ornaments.jpg "")

Exploring different puzzle tiling patterns / ornaments in Illustrator

![](./media/a4/a4_kerf_compensation_xy.jpg "")

Trick for adjusting both horizontal and vertical kerf compensation, using a stack of transforms around the offset function.

![](./media/a4/pieces.jpg "")

![](./media/a4/anim.gif "")

## Final results

I tried to find my inner child and remember the love for LEGOs in my childhood, and composed these two ... spaceships?

![](./media/a4/f01.jpg "")

![](./media/a4/f00.jpg "")

It was very tricky to equalize the fit in X, Y and Z directions. The beam of the laser is more elliptical than circular, 
so a square piece might have just slightly different width and height. Also the plywood thickness has a certain error margin / 
thickness tolerance depending on the manufacturer. After some tedious testing of ~10 batches I found a good combination of 
different interpretation for 6mm in each direction, which still has a bit more stiff fit in each orientation but is even 
enough for quick puzzle-like construction and deconstruction.