+++
title = "Assignment 6 - 3D Printing (group)"
description = "3D Printing (group)"
+++

## Group B - Dinan, Eero, Onni

## 1. Overhang (Eero)

Printer: Ultimaker 2+ Extended
Build volume: 223 x 220 x 205 mm
Filament: 3DK Berlin PLA 0.4mm 

![](./media/a6/01_ultimaker2e.jpg "")

The test piece design from Fab Academy features 10 straight-angle overhang elements (without building support from below), in 1mm length increments, ranging from 1 to 10 millimeters

![](./media/a6/02_cura.jpg "")

The Ultimaker CURA software highlights the areas in red if they should have a build support enabled in order to achieve good results

![](./media/a6/03_print.jpg "")

From the test print you can see that the 1mm extrusion / ledge looks still relatively ok, while the latter ones start to gather this "plastic spaghetti" below them as the printer is trying to build layers on top of thin air

## 2. Bridging (Onni)

I didn’t have time to use the 3D-printers at the lab this week so I used my own 3d printer, prusa mk3s. It has 250mmx210mmx210mm build area and I used some cheap basic black 1.75mm pla I had laying around.

![](./media/a6/04_printer.jpg "")

So I loaded the file into prusaslicer and this is how It looked, I used the default prusa settings for pla and 0.6mm nozzle. The bridging test has 10 bridges that increase in 2mm increments from 2mm to 20mm

![](./media/a6/05_slicing.jpg "")

It printed without any problems and there doesn’t seem to be any sagging even for the 20mm gap, it printed it really well.

![](./media/a6/06_print.jpg "")

## 2. Angles (Dinan)

Printer: LulzBot Mini
Build volume: 152 x 152 x 158 mm
Filament: 3DK Berlin PLA water blue 0.4mm 

Each LulzBot is connected to a Raspberry Pi. In Cura, I set the layer height to be 0.25mm and print speed to be 70 mm/s with support disabled. Then I saved gcode file to local and uploaded to web server to print. 

![](./media/a6/07_angles_cura.jpg "")
![](./media/a6/08_angles_cura2.jpg "")

After logging in on the web server, click “Connect”.

![](./media/a6/09_web.png "")

The first piece failed because it started to move while printing. After I did a bit of investigation with Solomon, we found that when I changed the material to water blue, I didn’t tighten the screws shown in the picture below. Due to that, the PLA thread coming out of the nozzle was not straight and that’s why the piece moved. To avoid the same mistake happening again, we can preheat the nozzle to target temperature and extrude about 10cm thread to check if it’s straight. 

![](./media/a6/10_print.jpg "")
![](./media/a6/11_nozzle.jpg "")

Here is the final outcome. We can see that when the angle is less than 10 degrees, the filament can’t support itself with “support” option disabled. 

![](./media/a6/12_final.jpg "")
![](./media/a6/13_final2.jpg "")
