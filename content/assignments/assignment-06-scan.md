+++
title = "Assignment 6 - 3D Scanning"
description = "3D Scanning"
+++

We did this assignment together with Dann, to make the scanning progress easier when the other person can operate the round scannable base plate.

*![](./media/a6_scan/00.jpg "")*

Scanner: Artec Eva

**Technical specs**

- 3D accuracy: Up to 0.1 mm
- 3D resolution: Up to 0.2 mm
- Object size: Starting from 10 cm
- Full color scanning
- Hybrid geometry and color based method
- 3D reconstruction rate: 16 FPS
- Output formats: All popular formats, including STL, OBJ, PLY, BTX
Weight of scanner: 0.9 kg


## Scan 1 - XBOX Gaming controller

*Size: approx. 150 x 100 x 60mm*

*![](./media/a6_scan/01.jpg "")*

*![](./media/a6_scan/02.jpg "")*

*Scanning progress in Artec Studio*

*![](./media/a6_scan/03.jpg "")*

The controller had some shiny plastic parts and a reflective center button that clearly affected the scanner light resulting in low quality.
But the biggest problem was that we didn’t enable the realtime fusion in the first scan.

## Scan 2 - 10L Watering can

Size approx. 40 x 20 x 60 cm

*![](./media/a6_scan/04.jpg "")*

*![](./media/a6_scan/05.jpg "")*

*![](./media/a6_scan/06.jpg "")*

Base removal using rectangular selection in orthogonal view

*![](./media/a6_scan/07.jpg "")*

Removing small artifacts by painting the selection.

*![](./media/a6_scan/08.jpg "")*

Final result after clean-up, and postprocessing: 
- small object filter
- hole filling
- mesh smoothing