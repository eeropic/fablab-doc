+++
title = "Assignment 6 - 3D Printing"
description = "3D Printing"
+++

To test the 3D printer, I created a simple custom knob for a potentiometer (6mm shaft)

The object has a hole for the potentiometer shaft, and a curved half-torus on top, to try out 
an object that couldn't be made easily only with subtractive methods.

![](./media/a6_2/knob1.jpg "")

## Modeling in Fusion 360

![](./media/a6_2/f00.jpg "")

Defining custom parameters for the design

![](./media/a6_2/f01.jpg "")

![](./media/a6_2/f02.jpg "")

![](./media/a6_2/f03.jpg "")

![](./media/a6_2/f04.jpg "")

![](./media/a6_2/f05.jpg "")

![](./media/a6_2/f06.jpg "")

## Importing the .STL in Ultimaker CURA software

![](./media/a6_2/00.jpg "")

![](./media/a6_2/01.jpg "")

![](./media/a6_2/02.jpg "")

## Printed piece

To achieve good fit for a knurled 6mm shaft, I should next make pieces with slight size adjustments, similar to kerf compensation in laser cutting, as the heating of the plastic and the resolution will affect how well the shaft will fit the knob.

The supports were easy to remove, although the the thing should have been printed upside down as now the top part is very low quality.

![](./media/a6_2/p00.jpg "")