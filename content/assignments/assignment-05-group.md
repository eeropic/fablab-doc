+++
title = "Assignment 5 - Electronics Production (group)"
description = "Electronics Production (group)"
+++

## Group B - Dinan, Eero, Onni

## Roland monoFab SRM-20

![](./media/a5/srm20.png "")

To setup the cutting bit in the machine you use a small allen key to tighten a small screw, you can open and close it to put and remove a cutting bit.

When making the path to cut we should use speeds 1mm/s with 0.3mm bit 1.5mm/s with 0.4mm and 2mm/s with 0.8mm. 

The 0.3mm and 0.4mm bits are useful when milling the copper to make the traces for the pcb and the 0.8mm works well when cutting the pcb outline. When using 0.3mm bits I think 5-6 offset is good, and 4 for 0.4mm bits, depending on the usage. When cutting the outline with a 0.8mm bit for example the cut depth could be  0.6mm and max depth 1.8mm so it will cut the 2mm thick pcb in three increments. We will want to use offset of 1 too so it will only cut 1 0.8mm wide 1.8mm deep outline.

![](./media/a5/srm-vpanel.png "")

At the start in the setup we should check that we are using the RML-1 command set.
In the Vpanel we can control the machine, we can move the toolend by pressing the arrows in the windows and then we can set the origin point in the upper right corner by pressing x/y or z. We can choose how fast we will move the toolend, you can choose continue or by 1/100mm *x increments, you can get really precise movement even manually in case it’s needed. For the spindle speed best to put it as high as possible without causing vibrations, in the high or second highest should be ok.
The z height is set by moving it to a position where it still has about 1cm room left to move downwards and then loosening the set screw with an allen key and pushing the bit against the part you want to cut. Then you can zero z-axis and tighten the screw.
After that you can press the cut button, choose your made file and cut it.

## Roland Modela MDX-40

![](./media/a5/mdx40.jpg "")

To set up the tool, we have to use the collet. To install on the machine, we turn it clockwise and tighten it with two wrenches. 

We use 0.4 mm tool to mill traces and 0.8 mm to mill outlines.

![](./media/a5/mdx-vpanel.jpg "")

We can press arrows on the user interface to move the tool. The spindle speed should be 9000 rpm. After we move the tool to starting point, we choose “Set XY Origin here” and press “Apply” to set xy origin. To set z origin, we use the sensor on the side of the machine. We put it on PCB board and make sure it’s right below the tool. Then we hit “Detect” and close the lid. The machine will move the tool down until it touches the sensor. We set the cutting speed below 50% at first and set back to 100% if the cutting is going well. 

![](./media/a5/mdx-2.jpg "")

Always check setup before cutting. We choose RML-1 since we use mods to generate the paths. 

To initialize cutting, we press “Cut”, choose the paths file and hit “Output”. 

After we finish cutting the traces, we change the 0.4mm tool to 0.8mm for outline. We don’t need to change xy origin but we have to reset z origin. 

## Mods

We use [mods](http://mods.cba.mit.edu) to convert png to rml file for milling. For outline, we click "mill outline" to get the default setting and manully enter the tool diamter 0.8, cut depth 0.6, max depth 1.8 and keep the offset number to 1.

![](./media/a5/mods1.jpg "")

For traces, we hit “mill traces” and choose tool diamter 0.4, cut depth 0.1, max depth 0.1. We set the offset number to 4.

![](./media/a5/mods2.jpg "")

Here is the original design as a .png file and our result:

![](./media/a5/linetest_original.png "")

![](./media/a5/linetest_final.jpg "")