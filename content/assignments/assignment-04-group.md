+++
title = "Assignment 4 - Computer-Controlled Cutting (group)"
description = "Computer-Controlled Cutting (group)"
+++

## Group B - Dinan, Eero, Onni

## Kerf measurement

In order to measure the kerf of the laser cutter in Aalto Fablab and also to know how thickness affects kerf, we cut testing pieces by using 2mm and 6mm plywood. 



First, we drew three 20 x 20 mm squares in CorelDraw. We used the recommended values on Notepad and set parameters of the laser cutter as following: 

Thickness of the Plywood | Speed/Power/Frequency
--- | ---
2 mm | 38 / 60 / 500
6 mm | 14 / 70 / 500

Below are the testing pieces we got for the 2mm plywood. We measured the length and width of each piece by using the digital vernier caliper.

![](./media/a4/IMG_6267.JPG "")

![](./media/a4/IMG_6268.JPG "")

![](./media/a4/IMG_6269.JPG "")

2mm Plywood | Piece 1 | Piece 2 | Piece 3 | Average | Kerf
 --- | --- | --- | --- | --- | --- |
Length (mm) | 19.88 | 19.78 | 19.87 | 19.84 | 0.08
Width (mm) | 19.74 | 19.74 | 19.73 | 19.74 | 0.13

So the kerf for 2mm plywood is 0.105 mm.


![](./media/a4/IMG_6274.JPG "")

The value would vary depending on how hard we press the vernier caliper. 
In order to minimize the human error, we aligned three pieces and measured them together. We got the following result:

2mm Plywood | Three Pieces | Average | Kerf
--- | --- | --- | ---
Length (mm) | 59.60 | 19.87 | 0.065
Width (mm) | 59.20 | 19.73 | 0.135

So the kerf for 2mm plywood is 0.10 mm.

Here are another 3 pieces we got of 6mm thickness. The laser did not cut all the way through although we used the recommended setting on the Notepad (Speed/Power/Frequency:14/70/500). 

![](./media/a4/IMG_6278.JPG "")

We applied some force and they popped out successfully. 

We went through the same process and got the following measurement values:

6mm Plywood | Piece 1 | Piece 2 | Piece 3 | Average | Kerf
--- | --- | --- | --- | --- | ---
Length (mm) | 19.92 | 19.77 | 19.79 | 19.83 | 0.085
Width (mm) | 19.73 | 19.78 | 19.76 | 19.76 | 0.12

So the kerf for 6mm plywood is 0.1025 mm.


As we mentioned above, the value would change if we compress the vernier caliper with different strength. Thus, we had the following result when we measured three pieces together:

6mm Plywood | Three Pieces | Average | Kerf
--- | --- | --- | ---
Length (mm) | 59.66-59.68 | 19.89 | 0.055
Width (mm) | 59.49-59.50 | 19.83 | 0.085
So the kerf for 6mm plywood is 0.07 mm.

Comparing kerfs we got for 2mm and 6mm plywood, we conclude that thickness of material would not change the kerf value. 


## Grayscale engraving test

We experimented engraving by using a grayscale strip. We set Speed/Power of raster to 90/50 based on the recommended parameter value on Notepad for 2mm plywood. 

![](./media/a4/IMG_6289.JPG "")

![](./media/a4/IMG_6290.JPG "")

We can see the contrast although it is not as high as the grayscale image.

## Speed & Power color mapping

Next, we explored how the speed and power affects the engraving outcome by using color mapping. We changed one parameter by 10% while keeping the other staying the same. 

First, we set the power level to be 50% and decreased the speed by 10% each starting from 100%. Doing this we got some contrast but only slightly.

![](./media/a4/IMG_6298.JPG "")

Second, we set the speed level to be 90% and decreased the power by 10% each starting from 70%. This way we got some good results and contrast, the contrast was bigger than with only varying the speed. If we would need to change only one value regarding contrast, it would be power.

![](./media/a4/IMG_6311.JPG "")

Finally, to maximize the contrast, we did a crossover setting as following:

![](./media/a4/IMG_6315.JPG "")

![](./media/a4/IMG_6317.JPG "")

They all have quite high contrast and we can tell the difference with bare eyes. We can feel the difference by touching as well. The darker the color, the deeper laser engraved. Overall we think by varying both of the values we can get the best contrast and results.





