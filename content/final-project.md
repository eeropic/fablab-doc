+++
title = "Final project"
description = "Final Project"
+++

## Idea 1
## Gyroscope rig for mobile 360° photo capture (2-axis)

- DIY 360° photo capture, for creating 3D environment maps
- For designers / game developers?
- Stitching with minimal error / without complex algorithms used with random erronous input data

### Notes
- First version only mechanical, with some kind of stepping mechanism for accurate angles (for example 10° steps in X & Y axis)
- 2nd version with stepper motors and shutter signal sent to phone automatically in each step
- Create microphone stand -compatible mount for the gyroscope
- Save phone gyroscope data somehow with the image for easier stitching / error handling
- Option to extract depth data from photos with depth-capable smartphones

### Challenges

- Managing level / level measure, use 3rd party "bubble level measure" ?


![Gyroscope sketch](./media/idea1_gyro.jpg)



## Idea 2
## Miniature stop motion photo studio

### Notes
- Controllable 180° "environment lightning" using RGB-led strips
- Top and front mounts for mobile phone (camera)
- Automated frame capture - take photo after N seconds when no motion is detected at the scen

![Miniature studio sketch](./media/idea2_stopmotion_studio.jpg)